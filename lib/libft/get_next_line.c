/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/13 15:34:43 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/01 12:01:59 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/libft.h"

static int		ft_read(const int fd, char **res)
{
	char	buff[BUFF_SIZE];
	char	*tmp;
	int		ret;

	if ((ret = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[ret] = '\0';
		tmp = *res;
		*res = ft_strjoin(*res, buff);
		ft_strdel(&tmp);
	}
	return (ret);
}

static void		ft_fill(char **res, char *newline)
{
	char	*tmp;

	tmp = *res;
	*res = ft_strdup(newline + 1);
	ft_strdel(&tmp);
}

static int		fd_free(char **res, int fd)
{
	ft_strdel(&(res[fd]));
	return (0);
}

int				get_next_line(const int fd, char **line)
{
	static char		*res[10000];
	char			*newline;
	int				read;

	if (fd < 0 || fd > 10000 || line == NULL || BUFF_SIZE > MAX_INT)
		return (-1);
	if (!res[fd])
		res[fd] = ft_strnew(1);
	while (!(newline = ft_strchr(res[fd], '\n')))
	{
		if ((read = ft_read(fd, &res[fd])) < 0)
			return (-1);
		if (read == 0 && !newline)
		{
			if (res[fd][0] == '\0')
				return (fd_free(res, fd));
			*line = ft_strdup(res[fd]);
			res[fd][0] = '\0';
			return (1);
		}
	}
	*line = ft_strsub(res[fd], 0, newline - res[fd]);
	ft_fill(&res[fd], newline);
	return (1);
}
